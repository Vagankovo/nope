package com.minecraftonline.nope.bridge.collision;

import net.minecraft.scoreboard.ScorePlayerTeam;

public interface ServerScoreboardBridge {
  ScorePlayerTeam nope$getDummyNoCollisionTeam();
}
