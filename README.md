# Nope

A volume protection plugin, based on LionsArea and WorldGuard.

## Contributing

This project uses Google standards for code styling.
You can apply real-time evaluation of your code for compliance with Google Checks
using a plugin. For IntelliJ-IDEA, you can download Checkstyle-IDEA and apply
Google Checks in the settings.

Commit early and commit often. When you submit a merge request (or push your changes directly),
each commit purpose should be well defined.